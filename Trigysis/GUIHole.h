#ifndef HOLE_H
#define HOLE_H

#include "CElement.h"
#include "GUIStarShip.h"

////////////////////////////////////////////////////
//**Hole
////////////////////////////////////////////////////

class Hole : public CElement
{

public:

	Hole(BasicInterface *super);
	virtual ~Hole();

	void SetFieldRadius(int fRadius) { this->FieldRadius; }
	int GetFieldRadius() { return this->FieldRadius; }

	void SetGravity(int gravity) { this->Gravity = gravity; }
	int GetGravity() { return this->Gravity; }

	bool GetIsAffecting(Element* pDestEl);

	virtual void OnMe() {}

	virtual bool Update() override;

private:

protected:


protected: 

	StarShip* Target;

	int FieldRadius;
	int Gravity;
	int Health;

	float LNotInRangeTime;

	float MinRange;

	bool NeedDestroy;

	float LDestroyTime;

};

////////////////////////////////////////////////////
//**SpaceeBall
////////////////////////////////////////////////////

class SpaceBall : public Hole
{

public:

	SpaceBall(BasicInterface* super);
	virtual ~SpaceBall();

	void SetIsBlack(bool isBlack) { this->IsBlack = isBlack; }

	virtual void OnMe() override;

private:

	bool IsBlack;

};

////////////////////////////////////////////////////
//**WhiteHole
////////////////////////////////////////////////////

class WhiteHole : public Hole
{

public:

	WhiteHole(BasicInterface* super);
	virtual ~WhiteHole();

	virtual bool Update() override;

	virtual void Destroy() override;

	virtual void OnMe() override;

private:


protected:


};

////////////////////////////////////////////////////
//**BlackHole
////////////////////////////////////////////////////

class BlackHole : public Hole
{

public:

	BlackHole(BasicInterface* super);
	virtual ~BlackHole();

	virtual void Destroy() override;

	virtual void OnMe() override;

	virtual bool Update() override;

private:

	bool ExStageTwo;
protected:


};

#endif //!HOLE_H