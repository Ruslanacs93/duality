#ifndef WORLD_H
#define WORLD_H

#include "GUIHole.h"
#include "GUIStarShip.h"

class World
{

public:

	World(BasicInterface* super, UINT indexOfVP);
	~World();

	void Update();

	void CreateWorld();
	void DeleteWorld();

	bool GetIsPlayerAlive() { return this->Player ? true : false; }

	void Render();

	void SaveHightScore();

private:

	void AddHoles(UINT maxDistance, UINT minDistance, UINT numOfHoles, __int8 blackPercent = 90);

private:

	int PScore;
	int BestScore;
	bool IsNewScoreBest;

	float LBarPosX;
	float TextOffSet;

	Element* BackGround;

	StarShip* Player;

	BasicInterface* D3dApp;
	UINT IndexOfVP;

	FileManager* ScoreFile;

};

#endif //!WORLD_H