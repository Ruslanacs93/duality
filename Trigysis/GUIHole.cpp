#include "GUIHole.h"

////////////////////////////////////////////////////////////
//**Hole
////////////////////////////////////////////////////////////
Hole::Hole(BasicInterface *super)
	: CElement(super)
{

	this->Gravity = 100;
	this->FieldRadius = 50;
	this->EName = "H";

}

Hole::~Hole()
{
	


}

bool Hole::GetIsAffecting(Element* pDestEl)
{

	if (!pDestEl)
		return false;

	if (Vector2d(pDestEl->GetPosition() - this->Position).GetLength() <= this->FieldRadius)
		return true;

	return false;

}



bool Hole::Update()
{

	if (!CElement::Update())
		return false;

	static std::vector<ElementInterface*> EFind;

	if (!this->Target)
	{

		EFind = this->PMLand->Find(std::string("SS"));

		if (!EFind.size())
			return true;

		this->Target = dynamic_cast<StarShip*>(EFind.at(0));

	}
	
	if (this->Target)
	{

		this->MinRange = Vector2d(this->D3dApp->GetWindowSizes().ClientWWidth,
			this->D3dApp->GetWindowSizes().ClientWHeight).GetLength() * 0.5f;


		if (!this->NeedDestroy && Vector2d(this->Position - this->Target->GetPosition()).GetLength() >= this->MinRange)
		{

			this->LNotInRangeTime = this->LNotInRangeTime + this->D3dApp->GetTimer()->GetDeltaTime();

			if (this->LNotInRangeTime >= 5)
			{

				this->Health = MAX_HEALTH;

				float Distance = MathHelp::GetRandom(this->MinRange + 1000, this->MinRange + 100, false);
				float Angle = MathHelp::GetRandom(D3DX_PI * 200, 0) / 100.f;

				this->SetPosition(Vector2d(Distance * sinf(Angle), Distance * cosf(Angle)));
				 
				this->LNotInRangeTime = 0;

			}

		}

		Vector2d Vec, NVec;

		Vec = this->Position - this->Target->GetPosition();

		if (this->Target->GetIsFired())
		{
			this->Target = nullptr;
		}
		else if (this->Gravity)
		{

			if (Vec.GetLength() < 500.f && Vec.GetLength() > 10.f)
			{
				NVec = Vec;
				NVec.Normalize();
				this->Target->AddImpulse(NVec * this->Gravity *500 / (Vec.GetLength()));

			}

		}

		if (Vec.GetLength() < this->FieldRadius)
			this->OnMe();

	}

	return true;


}

////////////////////////////////////////////////////
//**SpaceeBall
////////////////////////////////////////////////////

#define SP_SIZE 32
SpaceBall::SpaceBall(BasicInterface* super)
	:Hole(super)
{

	this->Gravity = 0;
	this->EName = "SB";

	this->IsBlack = MathHelp::GetRandom(1, 0, false);

	this->SetMaterial(std::string("SpaceBall"));
	this->SetSizes(Vector2d(SP_SIZE, SP_SIZE));
	this->SetBlendState(DX_BS_TRANSPARENCY);
	this->SetCustomVars(XMFLOAT4(this->IsBlack, 0, 0, 0));
	this->SetCameraAffect(1);

	this->FieldRadius = 32;

}
SpaceBall::~SpaceBall()
{



}

void SpaceBall::OnMe()
{

	if (this->Target)
	{

		this->Target->AddFuel(5);
		ElementDelete(this);

	}

}

//////////////////////////////////////////////////////
//**WhiteHole
//////////////////////////////////////////////////////

#define WHSIZE				100
#define WHGRAVITY			-15

#define WHITE_ADD_HEALTH	15
#define WHITE_ADD_FUEL		2

WhiteHole::WhiteHole(BasicInterface* super)
	: Hole(super)
{

	this->SetMaterial(std::string("WhiteHole"));
	this->Sizes = Vector2d(WHSIZE, WHSIZE);
	this->SetBlendState(DX_BS_TRANSPARENCY);

	this->Gravity = WHGRAVITY;
	this->FieldRadius = this->Sizes.X * 0.75f;

	this->SetCustomVars(XMFLOAT4(1, 0, 0, 0));	
	Precache(this);

}

WhiteHole::~WhiteHole()
{



}

void WhiteHole::Destroy()
{

	if (!this->NeedDestroy)
		if (this->Target)
		{

			this->Target->AddKill();
			this->Target->AddScore(-10);

		}
			

	this->NeedDestroy = true;

}

void WhiteHole::OnMe()
{

	if (this->Target)
	{
		if (this->NeedDestroy)
			return;
		this->Target->AddHealth(WHITE_ADD_HEALTH * this->D3dApp->GetTimer()->GetDeltaTime());
		this->Target->AddFuel(WHITE_ADD_FUEL * this->D3dApp->GetTimer()->GetDeltaTime());

	}

}

bool WhiteHole::Update()
{

	if (!Hole::Update())
		return false;

	if (this->NeedDestroy)
	{
		this->LDestroyTime = this->LDestroyTime + this->D3dApp->GetTimer()->GetDeltaTime();

		this->Rotation = this->Rotation + this->LDestroyTime * 100 * this->D3dApp->GetTimer()->GetDeltaTime();
		this->Sizes = this->Sizes + this->LDestroyTime*this->LDestroyTime * this->D3dApp->GetTimer()->GetDeltaTime() * 100;

		if (this->LDestroyTime >= 2)
		{

			ElementDelete(this);
			return false;
		}

		this->SetCustomVars(XMFLOAT4((2 - this->LDestroyTime) / 2, 0, 0, 0));

	}

	return true;

}

//////////////////////////////////////////////////////
//**BlackHole
//////////////////////////////////////////////////////

#define BHSIZE 100
#define BHGRAVITY 25
#define BLACK_DAMAGE 60

BlackHole::BlackHole(BasicInterface* super)
	: Hole(super)
{

	this->SetMaterial(std::string("BlackHole"));
	this->Sizes = Vector2d(BHSIZE, BHSIZE);
	this->SetBlendState(DX_BS_TRANSPARENCY);

	this->Gravity = BHGRAVITY;
	this->FieldRadius = this->Sizes.X * 0.5f;

	this->SetCustomVars(XMFLOAT4(0, 0, 0, 0));
	Precache(this);
}

BlackHole::~BlackHole()
{



}

void BlackHole::Destroy()
{

	if (!this->NeedDestroy)
		if (this->Target)
		{

			this->Target->AddKill();
			this->Target->AddScore(10);

		}

	this->NeedDestroy = true;

}

void BlackHole::OnMe()
{

	if (this->Target)
	{
		if (this->NeedDestroy)
			return;
		this->Target->Damage(BLACK_DAMAGE * this->D3dApp->GetTimer()->GetDeltaTime());

	}

}

bool BlackHole::Update()
{

	if (!Hole::Update())
		return false;


	if (this->NeedDestroy)
	{
		this->LDestroyTime = this->LDestroyTime + this->D3dApp->GetTimer()->GetDeltaTime();

		if (this->LDestroyTime < 0.5f)
		{
			this->Sizes = this->Sizes - 256 * this->D3dApp->GetTimer()->GetDeltaTime();
			this->SetCustomVars(XMFLOAT4(0, 0, this->LDestroyTime * 2, 0));
		}
		else if (this->LDestroyTime >= 0.5f)
		{
			
			if (this->LDestroyTime >= 2)
			{
				ElementDelete(this);
				return false;
			}

			if (!this->ExStageTwo)
			{
				this->ExStageTwo = true;
				this->SetMaterial(std::string("BlackHoleExplose"));
			}

			float Width = -1 / (this->LDestroyTime - 0.45f)*50.f + 256;

			this->Sizes = Vector2d(Width, Width);
			
			this->SetCustomVars(XMFLOAT4(1, 1, 1 - this->Sizes.X / 256, 0));

		}
		
		if (this->LDestroyTime >= 2)
		{
			ElementDelete(this);
			return false;
		}

		//this->SetCustomVars(XMFLOAT4((2 - this->LDestroyTime) / 2, 0, 0, 0));

	}

	return true;

}