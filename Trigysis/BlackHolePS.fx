//#include "PSInputs.fx"


SamplerState Sampler : register(s0);

Texture2D Texture : register(t0);
Texture2D ATexture : register(t1);

cbuffer CPPOutput : register(b0)
{
    // 8 + 4 + 4 = 16 
    float Time;
    float DeltaTime;
    uint UseAlpha;
    float Pad;
    float UVal0;
    float UVal1;
    float UVal2;
    float UVal3;

};

struct PSInput
{

    float4 PosH : SV_POSITION;
    float2 PosW : POS;
    float4 Color : COLOR0;
    float4 AColor : COLOR1;
    float2 TexCoord : TEXCOORD0;
    float2 ATexCoord : TEXCOORD1;

};
float4 PSMain(PSInput input) : SV_Target
{
	
    float Len = length(input.PosW);

    if (!UVal0)
    {
        float4 PTextireX1 = Texture.Sample(Sampler, input.TexCoord +
        float2(sin(input.TexCoord.x) * 0.1f, sin(Time + 50 * input.TexCoord.x) * 0.02f));

        float4 PTextireX2 = Texture.Sample(Sampler, float2(128, 0) - (input.TexCoord +
        float2(sin(input.TexCoord.x) * 0.1f, sin(-Time + 25 * input.TexCoord.x) * 0.034f)));

        PTextireX1 *= PTextireX2;

        return PTextireX1 * (1.2f).rrrr * (1 +  UVal2 * 20);
    }

    float4 PTextire = Texture.Sample(Sampler, input.TexCoord);
    
    return float4(PTextire.rgb, PTextire.a * UVal2 - 0.1f);

}