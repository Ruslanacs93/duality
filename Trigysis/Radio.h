#ifndef RADIO_H
#define RADIO_H

#include "Sound.h"

#define RADIO_STATUS_ON 0x1
#define RADIO_STATUS_FAILED 0x2

class Radio
{

public:
	Radio(SoundBasic* bSound, std::string& path);
	~Radio();

	void Toggle();
	void VolumeUp(short speed);
	void SwitchToNext(bool reverse = false);

	__int8 GetStatus() { return this->Status; }
	__int16 GetIndexOfCSound() { return this->IndexOfCSound; }

	void Update();

private:

	bool Init();

private:

	__int8 Status;
	__int16 IndexOfCSound;
	__int16 Volume;

	std::string Path;

	IDirectSoundBuffer8* CSound;

	std::vector<IDirectSoundBuffer8*> SoundList;

	SoundBasic* BSound;

};

#endif //!RADIO_H