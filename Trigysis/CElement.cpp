#include "CElement.h"

CElement::CElement(BasicInterface* super)
	:Element(super)
{

	this->Health = MAX_HEALTH;

}

CElement::~CElement()
{



}

void CElement::Destroy()
{

	this->NeedDestroy = true;

}

bool CElement::Update()
{

	if (!Element::Update())
		return false;

	if (this->Health <= 0)
	{
		this->Destroy();
	}

	return true;

}

void CElement::Damage(float damage)
{

	this->Health = this->Health - damage;
	if (this->Health < 0)
		this->Health = 0;

}