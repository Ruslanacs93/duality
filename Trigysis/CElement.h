#ifndef C_ELEMENT_H
#define C_ELEMENT_H

#include "Element.h"

#define MAX_HEALTH 100.f

class CElement : public Element
{

public:

	CElement(BasicInterface* super);
	virtual ~CElement();

	virtual void Damage(float damage);
	virtual void Destroy();

	virtual bool Update() override;

	void SetHealth(float health) { this->Health = health; }
	int GetHealth() { return this->Health; }

protected:
	bool NeedDestroy;
	float Health;

};

#endif //!C_ELEMENT_H