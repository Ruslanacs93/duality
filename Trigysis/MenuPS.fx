//#include "PSInputs.fx"


SamplerState Sampler : register(s0);

Texture2D Texture : register(t0);
Texture2D ATexture : register(t1);

cbuffer CPPOutput : register(b0)
{
    // 8 + 4 + 4 = 16 
    float Time;
    float DeltaTime;
    uint UseAlpha;
    float Pad;
    float UVal0;
    float UVal1;
    float UVal2;
    float UVal3;

};

struct PSInput
{

    float4 PosH : SV_POSITION;
    float2 PosW : POS;
    float4 Color : COLOR0;
    float4 AColor : COLOR1;
    float2 TexCoord : TEXCOORD0;
    float2 ATexCoord : TEXCOORD1;

};
float4 PSMain(PSInput input) : SV_Target
{
	

    float4 PTexture = Texture.Sample(Sampler, input.TexCoord);


    if (abs(input.PosW.x) < 100)
    {
        
        float4 PDTexture = Texture.Sample(Sampler, input.TexCoord + float2(sin((Time*0.5f + input.TexCoord.y)*10)*0.001f, 0));
        return lerp(PDTexture, PTexture, abs(input.PosW.x / 100 ));
        //return float4(1, 0, 0, 1);
    }

    return PTexture;

}