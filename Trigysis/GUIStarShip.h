#ifndef STAR_SHIP_H
#define STAR_SHIP_H

#include "CElement.h"
#include "Radio.h"

class Bullet;

////////////////////////////////////////////////////
//**StarShip
////////////////////////////////////////////////////

#define FIER_DELAY 0.25f

class StarShip : public CElement
{

public:

	StarShip(BasicInterface* super);
	virtual ~StarShip();

	virtual void Move();
	virtual void RadioControl();

	virtual bool Update() override;

	void SetImpulse(Vector2d& impulse) { this->Impulse = impulse; }
	Vector2d& GetImpulse() { return this->Impulse; }
	Vector2d& GetNImpulse() { return this->NImpulse; }
	
	void AddImpulse(Vector2d& addImpulse) { this->Impulse = this->Impulse + addImpulse; }

	void ResetImpulse() { this->Impulse = Vector2d(0, 0); }

	void AddFuel(float fuelToAdd);
	float GetFuel() { return this->Fuel; }

	int GetScore() { return this->Score; }
	void AddScore(int scoreToAdd);

	void AddHealth(float healthToAdd);

	void AddKill(){ this->Kills++; }
	UINT GetKills() { return this->Kills; }
	void ResetKills() { this->Kills = 0; }

	Radio* GetRadio() { return this->SRadio; }

private:

	float FlameOffset;

	Vector2d PseudoPos;

	Element* EFlame;

	float LFireDelay;
	float LVolDT;

protected:

	virtual void Shoot();

protected:

	UINT Kills;

	float LDestroyTime;

	float Force;
	float Fuel;

	int Score;

	float Speed;

	Vector2d Impulse;
	Vector2d NImpulse;

	Radio* SRadio;

};

////////////////////////////////////////////////////
//**Bullet
////////////////////////////////////////////////////

#define BULLET_SPEED 500
#define BULLET_LIFETIME 2

class Bullet : public Element
{

public:

	Bullet(BasicInterface* super);
	virtual ~Bullet();

	void SetParentClass(Element* pParentClass) { this->ParentClass = pParentClass; }

	virtual bool Update() override;
	virtual void Spawn(Vector2d& position, short indexOfVPort) override;

private:

	void Hit();

	Vector2d StartImpulse;

	float LLifeTime;

	Element* ParentClass;

};

#endif //!STAR_SHIP_H