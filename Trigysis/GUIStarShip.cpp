#include "GUIStarShip.h"

////////////////////////////////////////////////////
//**StarShip
////////////////////////////////////////////////////

#define SIZES 64
#define FLAME_OFFSET_LENGTH 27
#define FLAME_OFFSET Vector2d(0, FLAME_OFFSET_LENGTH)
#define MAX_FORCE 100
#define MAX_FUEL 100
#define ROTATE_SPEED 4

#define VOLUME_MIN_DT 0.1f

StarShip::StarShip(BasicInterface* super)
	:CElement(super)
{

	this->Sizes = Vector2d(SIZES, SIZES);
	this->SetMaterial(std::string("StarShip"));

	this->Impulse = Vector2d(0, 0);

	this->CameraAffect = 0;

	this->Fuel = 100;
	
	this->EFlame = new Element(super);
	this->EFlame->SetSizes(this->Sizes);
	this->EFlame->SetBlendState(DX_BS_TRANSPARENCY);
	this->EFlame->SetMaterial(std::string("ShipFlame"));
	this->EFlame->SetCameraAffect(0);
	this->SRadio = new Radio(this->Super->GetSound(), this->Super->GetCatalogName() + "\\Resources\\Sounds\\Radio");

	this->FlameOffset = FLAME_OFFSET_LENGTH;

	this->LFireDelay = FIER_DELAY;

	this->EName = "SS";

	Precache(this);

}
StarShip::~StarShip()
{

	D3DDelete(this->SRadio);

}

void StarShip::RadioControl()
{

	if (!this->SRadio)
		return;

	if (this->Input->KBClicked('Y'))
	{
		this->SRadio->Toggle();
	}

	if (this->Input->KBClicked(VK_NUMPAD8))
	{
		this->SRadio->VolumeUp(10);
	}

	if (this->Input->KBClicked(VK_NUMPAD2))
	{
		this->SRadio->VolumeUp(-10);
	}

	if (this->Input->KBClicked(VK_NUMPAD6))
	{
		this->SRadio->SwitchToNext();
	}

	if (this->Input->KBClicked(VK_NUMPAD4))
	{
		this->SRadio->SwitchToNext(true);
	}

}

void StarShip::Shoot()
{

	if (this->LFireDelay >= FIER_DELAY)
	{

		if (this->Score)
			this->Score--;

		Bullet* NewBullet = new Bullet(this->Super);

		NewBullet->SetParentClass(this);
		NewBullet->SetRotation(this->Rotation);
		NewBullet->Spawn(this->Position, this->IndexOfViewPort);
		this->Super->GetSound()->Play(std::string("sound01.wav"), 50);

		this->LFireDelay = 0;
	}

}

void StarShip::Move()
{

	static float DeltaTime;
	DeltaTime = this->D3dApp->GetTimer()->GetDeltaTime();

	if (GetAsyncKeyState(VK_LEFT) & 0x8000)
		this->Rotation = this->Rotation + ROTATE_SPEED * DeltaTime;

	if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
		this->Rotation = this->Rotation - ROTATE_SPEED * DeltaTime;

	if ((GetAsyncKeyState(VK_SHIFT) & 0x8000) && this->Fuel > 0)
	{

		this->Force = this->Force + MAX_FORCE * 2 * DeltaTime;
		if (this->Force > MAX_FORCE)
			this->Force = MAX_FORCE;

		this->Fuel = this->Fuel - 5.f * DeltaTime;
		if (this->Fuel < 0)
			this->Fuel = 0;

	}
	else
	{

		this->Force = this->Force - MAX_FORCE * DeltaTime;
		if (this->Force < 0)
			this->Force = 0;

	}
	
	this->NImpulse = Vector2d(-sinf(this->Rotation), cosf(this->Rotation));

	this->Impulse = this->Impulse + this->NImpulse * this->Force;

	this->PseudoPos = this->PseudoPos + this->Impulse * DeltaTime;

	this->D3dApp->GetVPStruct(this->IndexOfViewPort).VPCamera.SetPosition(this->PseudoPos);

	this->Speed = this->Impulse.GetLength();

}

Vector2d PPos = Vector2d(0,0);
bool StarShip::Update()
{

	if (!Element::Update())
		return false;
	
	if (this->NeedDestroy)
	{

		this->LDestroyTime = this->LDestroyTime + this->D3dApp->GetTimer()->GetDeltaTime();

		
		ElementDelete(this->EFlame);
		ElementDelete(this);
		return false;
		


	}

	this->LFireDelay = this->LFireDelay + this->D3dApp->GetTimer()->GetDeltaTime();

	this->Move();
	this->RadioControl();
	if (GetAsyncKeyState(VK_SPACE) & 0x8000)
		this->Shoot();

	this->EFlame->SetPosition(this->Position - this->NImpulse * this->FlameOffset);
	this->EFlame->SetRotation(this->Rotation);
	this->EFlame->SetCustomVars(XMFLOAT4(this->Force / MAX_FORCE, 0, 0, 0));

	if (!this->EFlame->GetIsSpawned())
		this->EFlame->Spawn(this->Position, this->IndexOfViewPort);

	PPos = this->PseudoPos ;

	this->SRadio->Update();

	if (this->Health <= 0)
		this->Destroy();

}

void StarShip::AddFuel(float fuelToAdd)
{

	this->Fuel = this->Fuel + fuelToAdd;

	if (this->Fuel > MAX_FUEL)
		this->Fuel = MAX_FUEL;

}

void StarShip::AddScore(int scoreToAdd)
{

	this->Score = this->Score + scoreToAdd;

	if (this->Score < 0)
		this->Score = 0;

}

void StarShip::AddHealth(float healthToAdd)
{

	this->Health = this->Health + healthToAdd;

	if (this->Health < 0)
		this->Health = 0;

	if (this->Health > MAX_HEALTH)
		this->Health = MAX_HEALTH;

}

////////////////////////////////////////////////////
//**Bullet
////////////////////////////////////////////////////
Bullet::Bullet(BasicInterface* super)
	:Element(super)
{

	this->Sizes = Vector2d(32, 32);
	this->SetMaterial(std::string("Bullet"));
	this->SetBlendState(DX_BS_TRANSPARENCY);
	this->SetCameraAffect(0);

}

Bullet::~Bullet()
{



}

void Bullet::Spawn(Vector2d& position, short indexOfVPort)
{

	Element::Spawn(position, indexOfVPort);

	if (this->ParentClass)
		this->StartImpulse = reinterpret_cast<StarShip*>(this->ParentClass)->GetNImpulse();

	this->LLifeTime = 0;

}

void Bullet::Hit()
{

	static std::vector<ElementInterface*> EFind;

	EFind = this->PMLand->Find(std::string("H"));
	if (!EFind.size())
		return;

	for (int i = 0; i < EFind.size(); i++)
	{
		if (!EFind.at(i))
			continue;
		if (Vector2d(EFind.at(i)->GetPosition() - this->Position).GetLength() <= 64)
		{
			dynamic_cast<CElement*>(EFind.at(i))->Damage(40);
			ElementDelete(this);
			return;
		}

	}

}

bool Bullet::Update()
{

	if (!Element::Update())
		return false;

	if (this->ParentClass)
		if (this->ParentClass->GetIsFired())
			this->ParentClass = nullptr;

	this->LLifeTime = this->LLifeTime + this->D3dApp->GetTimer()->GetDeltaTime();

	if (this->LLifeTime >= BULLET_LIFETIME)
	{
		ElementDelete(this);
		return false;
	}
		
	if (BULLET_SPEED)
	{

		this->Position = this->Position + (this->StartImpulse * BULLET_SPEED) *
			this->D3dApp->GetTimer()->GetDeltaTime();

	}

	this->Hit();

	return true;

}
