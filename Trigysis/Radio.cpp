#include "Radio.h"
#include "MathHelper.h"

Radio::Radio(SoundBasic* bSound, std::string& path)
{
	this->IndexOfCSound = 0;
	this->Status = 0x0;
	this->Volume = 80;

	this->BSound = bSound;

	this->Path = path;

	if (!this->Init())
		this->Status |= RADIO_STATUS_FAILED;

}
Radio::~Radio()
{

	this->BSound->Stop(this->CSound);

}

bool Radio::Init()
{

	if (!this->BSound)
		return false;

	std::vector<std::string>* Files;

	bool Success = this->BSound->LoadSounds(this->Path, &Files);
	if (!Files || !Success)
		return false;

	__int16 FilesSize = Files->size();
	__int16 Index = 0;

	for (__int16 i = 0; i < FilesSize; i++)
	{

		Index = MathHelp::GetRandom(Files->size() - 1, 0, false);
		this->SoundList.push_back(this->BSound->GetSound(Files->at(Index)));
		Files->erase(Files->begin() + Index);

	}

	this->CSound = this->SoundList.at(0);
	return true;
}

void Radio::Toggle()
{

	if (this->Status & RADIO_STATUS_ON)
	{
		this->BSound->Stop(this->CSound);
	}
	else
	{
		this->BSound->Play(this->CSound, this->Volume);
	}
	this->Status ^= RADIO_STATUS_ON;

}

void Radio::VolumeUp(short speed)
{

	this->Volume = this->Volume + speed;
	this->Volume = SLIMIT(this->Volume, 0, 100);

	this->BSound->SetVolume(this->CSound, this->Volume);

}

void Radio::SwitchToNext(bool reverse)
{

	if (!(this->Status & RADIO_STATUS_ON) || (this->Status & RADIO_STATUS_FAILED))
		return;

	this->Toggle();
	if (!reverse)
		this->IndexOfCSound++;
	else
		this->IndexOfCSound--;
	this->IndexOfCSound = this->IndexOfCSound % this->SoundList.size();
	this->CSound = this->SoundList.at(this->IndexOfCSound);
	this->Toggle();

}

void Radio::Update()
{

	if (this->Status & RADIO_STATUS_FAILED)
		return;

	if (!(this->BSound->GetIsPlaying(this->CSound)) && (this->Status & RADIO_STATUS_ON))
		this->SwitchToNext();

}