#ifndef Duality_H
#define Duality_H

#include "ElementBaseManager.h"
#include "D3DAPP.h"
#include "Element.h"
#include "Button.h"
#include "BasicInterface.h"
#include "GUIStarShip.h"
#include "World.h"
//#include "LogicalElement.h"
#include "Basic2DDraw.h"
//#include "Link.h"
#include "Files.h"

//#include "TestPipeline.h"

class Button : public ButtonInterface 
{
public:
	Button(BasicInterface* super, ButtonSettings& bs = ButtonSettings());

};

struct BSProperties
{
	float Step;
	int AreaWidth;
	int AreaHeight;
	int BWidth;
	int BHeight;
};

class ButtonSlider : public Element
{

public:

	ButtonSlider(BasicInterface* super, float initVal);
	~ButtonSlider();
	void Spawn(Vector2d& position, short indexOfVP) override;
	bool Update() override;

	float GetValue() { return this->Value; }
	void SetValue(float val) { this->Value = val; }

	bool GetIsChanging() { return this->IsChanging; }

	bool GetIsReleased() { return this->PButton->GetStatus(DX_BUTTON_STATUS_WAS_PRESSED); }
	void SetPrimaryDescText(Vector2d& pos, float scale, char* text);
	void EnableOutValText(Vector2d& pos, float scale, float coeff, bool isFloat = false);


private:

	Vector2d PrevBPos;
	float InitVal;

	ButtonInterface* PButton;
	BSProperties Props;

	float Value;
	bool IsChanging;

	Vector2d PrevMousePos;
	Vector2d MousePos;

	Vector2d VTPos;
	float VTScale;
	float VTCoeff;
	bool IsVTFloat;

};

//////////////////////////////////////////////////////
//*Duality
//////////////////////////////////////////////////////

#define STATUS_MENU 1
#define STATUS_GAME 2 
#define STATUS_PAUSE 3
#define STATUS_GAME_PAUSE 4
#define STATUS_PAUSE_GAME 5
#define STATUS_PAUSE_MENU 6
#define STATUS_MENU_GAME 7
#define STATUS_START_MENU 8
#define STATUS_GAME_OVER 9
#define STATUS_SETTINGS 10

class Duality : public BasicInterface
{
public:
	Duality(HWND hWnd, D3DAPPINPUT* input) : BasicInterface(false, true, hWnd, input)
	{}
	void ReleaseDefault() override;
	bool InitApp();
	void Update();
	void InitVPorts();
	void Resize(enum EDisplayModes mode) override;
	char GetStatus() { return this->Status; }
	void SetStatus(char status) { this->Status = status; }
	World* GetSpace() { return this->Space; }
//private:
	/////////////////////////////
	//**Functions for buttons
	/////////////////////////////

	//TestPipeline* TPipeline;

	void WorkWithButtons();

	
	void BToggleResButtons();
	void CheckResButtons();

	void GameUpdate();

	void InitPause();
	void InitMenu();
	void InitGame();
	void InitGameOver();

	void Settings();

	void ClearMButtons();

	////////////////////////////////
	//**Buttons
	////////////////////////////////

	std::vector<Button*>& GetMButtons() { return this->MButtons; }

	void InitBS();
	void DeleteBS();
	__int16 GetMenuVolume() { return this->MenuVolume; }

private:

	std::vector<Hole*> Holes;

	World* Space;
	StarShip* PlayerShip;

	std::vector<Button*> MButtons;

	char Status;

	ButtonSlider* TestBS;

private:

	__int16 MenuVolume;
	__int16 UIVolume;
	__int16 GameEffectsVolume;

	float Brightness;
	float CSaturation;

};
#endif //Duality_H
