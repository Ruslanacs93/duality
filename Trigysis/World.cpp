#include "World.h"

World::World(BasicInterface* super, UINT indexOfVP)
{

	this->D3dApp = super;
	if (this->D3dApp)
	{

		this->ScoreFile = new FileManager(this->D3dApp->GetCatalogName() + "\\Score.txt");


		if (indexOfVP < this->D3dApp->GetNumOfVPorts())
		{
			this->IndexOfVP = indexOfVP;
		}
		else
			indexOfVP = -1;

		this->LBarPosX = -290 * this->D3dApp->GetSizeKoeff();
		this->TextOffSet = -70;

	}

}

World::~World()
{

	D3DDelete(this->ScoreFile);

}

void World::CreateWorld()
{

	if (!this->D3dApp || this->IndexOfVP < 0)
		return;

	this->BackGround = new Element(this->D3dApp);
	this->BackGround->SetSizes(Vector2d(this->D3dApp->GetWindowSizes().ClientWWidth, this->D3dApp->GetWindowSizes().ClientWHeight));
	this->BackGround->SetMaterial(std::string("Space"));
	this->BackGround->SetCameraAffect(0); //0.005f
	this->BackGround->Spawn(Vector2d(0, 0), 1);

	this->AddHoles(2000, 200, 50);

	this->Player = new StarShip(this->D3dApp);
	this->Player->Spawn(Vector2d(0, 0), this->IndexOfVP);

	this->ScoreFile->Open(FM_FILE_READ);
	std::string Str = this->ScoreFile->GetStringFromFile();
	if (!Str.compare(""))
	{
		this->BestScore = 0;
	}
	else
	{
		this->BestScore = StringHelp::ASCIIToINT(Str);
	}
	this->ScoreFile->Close();

}
void World::DeleteWorld()
{

	this->D3dApp->GetElementBase()->ReleaseCache();

	for (int i = 0; i < this->D3dApp->GetElementBase()->GetElements(this->IndexOfVP).size(); i++)
	{

		ElementDelete(this->D3dApp->GetElementBase()->GetElements(this->IndexOfVP).at(i));

	}

}

void World::AddHoles(UINT maxDistance, UINT minDistance, UINT numOfHoles, __int8 blackPercent)
{

	Vector2d CPos;
	bool IsBlackHole = false;
	CPos = this->D3dApp->GetVPStruct(this->IndexOfVP).VPCamera.GetPosition();

	Hole* NewHole = nullptr;

	for (int i = 0; i < numOfHoles; i++)
	{
		if (MathHelp::GetRandom(9, 0, false) > 10 - blackPercent / 10)
			NewHole = new BlackHole(this->D3dApp);
		else
			NewHole = new WhiteHole(this->D3dApp);

		NewHole->Spawn(Vector2d(MathHelp::GetRandom(maxDistance, minDistance), MathHelp::GetRandom(maxDistance, minDistance)) + CPos, this->IndexOfVP);

	}

	for (int i = 0; i < numOfHoles * 1.25f; i++)
	{

		SpaceBall* NSB = new SpaceBall(this->D3dApp);
		NSB->Spawn(Vector2d(MathHelp::GetRandom(maxDistance, minDistance), MathHelp::GetRandom(maxDistance, minDistance)), this->IndexOfVP);

	}

}

void World::Update()
{

	if (!this->D3dApp || this->IndexOfVP < 0)
		return;

	if (this->Player)
	{
		if (this->Player->GetKills() >= 10)
		{

			float MinRange = Vector2d(this->D3dApp->GetWindowSizes().ClientWWidth, 
				this->D3dApp->GetWindowSizes().ClientWHeight).GetLength() * 0.5f;

			this->AddHoles(MinRange + 2000, MinRange + 100, 15, 100);

			this->Player->ResetKills();

		}

		if (this->Player->GetIsFired())
		{
			this->PScore = this->Player->GetScore();

			if (this->PScore > this->BestScore)
			{
				this->IsNewScoreBest = true;
				this->ScoreFile->Open(FM_FILE_WRITE);
				this->ScoreFile->GetFILE() << this->PScore;
				this->ScoreFile->Close();
			}
			else
				this->IsNewScoreBest = false;

			this->Player = nullptr;
		}
	}

	this->BackGround->GetMaterial()->TextureOffset.x = this->BackGround->GetMaterial()->TextureOffset.x +
		this->D3dApp->GetVPStruct(this->IndexOfVP).VPCamera.GetDeltaPosition().X * 0.000025f * this->D3dApp->GetTimer()->GetDeltaTime();

	this->BackGround->GetMaterial()->TextureOffset.y = this->BackGround->GetMaterial()->TextureOffset.y -
		this->D3dApp->GetVPStruct(this->IndexOfVP).VPCamera.GetDeltaPosition().Y * 0.000025f * this->D3dApp->GetTimer()->GetDeltaTime();

}

void World::SaveHightScore()
{
	
	if (this->PScore > this->BestScore)
	{
		this->ScoreFile->Open(FM_FILE_WRITE);
		this->ScoreFile->GetFILE() << this->PScore;
		this->ScoreFile->Close();
	}

}

void World::Render()
{

	if (!this->Player)
	{
		this->D3dApp->GetFont2D()->DrawA(Vector2d(-95, 120), COLOR_RED_3, 1.f, "GAME OVER!");
		this->D3dApp->GetFont2D()->DrawA(Vector2d(-105, 90), COLOR_WHITE_3, 1, "Your score: %d", this->PScore);
		if (this->IsNewScoreBest)
		{
			this->D3dApp->GetFont2D()->DrawA(Vector2d(-105, 60), COLOR_GREEN_3, 1, "New best score!");

		}
		else
		{
			this->D3dApp->GetFont2D()->DrawA(Vector2d(-105, 60), COLOR_WHITE_3, 1, "Best score is %d", this->BestScore);
		}
	}
	else
	{
		this->D3dApp->GetDraw2D()->DrawRectangle(Vector2d(this->LBarPosX, -232),
			0, Vector2d(196, 32), this->IndexOfVP, XMFLOAT4(this->Player->GetHealth() / 100.f, 0, 0, 0),
			XMFLOAT4(1, 0.5, 0.5, 0), this->D3dApp->GetMaterial(std::string("LineBar")));

		this->D3dApp->GetDraw2D()->DrawRectangle(Vector2d(this->LBarPosX, -200),
			0, Vector2d(196, 32), this->IndexOfVP, XMFLOAT4(this->Player->GetFuel() / 100.f, 0, 0, 0),
			XMFLOAT4(0.5, 0.5, 1, 0), this->D3dApp->GetMaterial(std::string("LineBar")));

		this->D3dApp->GetFont2D()->DrawA(Vector2d(this->LBarPosX + this->TextOffSet, -232), COLOR_WHITE_3, 1.f, "Health");
		this->D3dApp->GetFont2D()->DrawA(Vector2d(this->LBarPosX + this->TextOffSet, -200), COLOR_WHITE_3, 1.f, "Fuel");
		this->D3dApp->GetFont2D()->DrawA(Vector2d(-60, 200), COLOR_WHITE_3, 1.f, "Score: %d", this->Player->GetScore());

	}

}