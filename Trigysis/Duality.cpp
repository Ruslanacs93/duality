#include "Duality.h"

//////////////////////////////////////////////////////
//**Button
//////////////////////////////////////////////////////

#define BSIZES Vector2d(196,32)
#define COLOR_BTEXT XMFLOAT3(0.1,0.1,0.5)

Button::Button(BasicInterface* super, ButtonSettings& bs)
:ButtonInterface(super, bs)
{
	DeclareElementName(CCButton, this->EName);
	this->Sizes = BSIZES;
	this->SetBlendState(DX_BS_TRANSPARENCY);

	ButtonSettings SBS;
	SBS.IdleColor = XMFLOAT4(0.1, 0.1, 0.5, 0);
	SBS.InActiveColor = XMFLOAT4(0, 0, 0.25f, 0);
	SBS.MaterialName = std::string("Button");
	SBS.ReadyColor = XMFLOAT4(0.25, 0.25, 0.25, 0);
	SBS.SuppressColor = XMFLOAT4(0, 0, 0, 0);

	this->SetSettings(SBS);
	//this->SetShapeType(DX_)
}

/////////////////////////////////////////////////////
//**ButtonSlider
/////////////////////////////////////////////////////

ButtonSlider::ButtonSlider(BasicInterface* super, float initVal)
	:Element(super)
{

	this->Props.AreaHeight = BSIZES.Y;
	this->Props.AreaWidth = BSIZES.X - 35;
	this->Props.BWidth = 12;
	this->Props.BHeight = 8;
	this->Props.Step = 0.1f;

	ButtonSettings BS;
	BS.MaterialName = std::string("ButtonSliderM");
	BS.IdleColor = XMFLOAT4(0.15, 0.15, 0.15, 1);
	BS.InActiveColor = XMFLOAT4(0, 0, 0, 1);
	BS.ReadyColor = XMFLOAT4(0, 0, 0, 1);
	BS.SuppressColor = XMFLOAT4(0, 0, 0, 1);
	this->PButton = new ButtonInterface(super, BS);
	this->PButton->SetSizes(Vector2d(Props.BWidth, Props.BHeight));

	this->SetBlendState(DX_BS_TRANSPARENCY);
	this->SetMaterial(std::string("ButtonSlider"));
	this->SetSizes(BSIZES);
	this->InitVal = initVal;

	this->TIsConstant = true;

}
ButtonSlider::~ButtonSlider()
{
	ElementDelete(this->PButton);
}
void ButtonSlider::Spawn(Vector2d& position, short indexOfVP)
{
	Element::Spawn(position, indexOfVP);


	this->Value = (this->PButton->GetPosition().X - this->Position.X + this->Props.AreaWidth * 0.5f) / this->Props.AreaWidth;

	this->PButton->Spawn(Vector2d((this->InitVal * this->Props.AreaWidth + this->Position.X - this->Props.AreaWidth * 0.5f), this->Position.Y), indexOfVP);
	
	this->Value = (this->PButton->GetPosition().X - this->Position.X + this->Props.AreaWidth * 0.5f) / this->Props.AreaWidth;
}

bool ButtonSlider::Update()
{

	if (!Element::Update())
		return false;


	if (this->PButton->GetStatus(DX_BUTTON_STATUS_IS_PRESSING))
	{
		if (!this->IsChanging)
		{
			this->Input->GetMousePosCenterVPort(this->Super->GetVPStruct(1), &this->PrevMousePos);
			//PrevMPos = TMPrevPos;
			this->IsChanging = true;
		}
		else
			this->PrevMousePos = this->MousePos;

		this->Input->GetMousePosCenterVPort(this->Super->GetVPStruct(1), &this->MousePos);
		this->PButton->SetPosition(Vector2d(this->PButton->GetPosition().X + this->MousePos.X - this->PrevMousePos.X,
			this->Position.Y));

		if (this->PButton->GetPosition().X < (this->Position.X - this->Props.AreaWidth*0.5f))
			this->PButton->SetPosition(Vector2d(this->Position.X - this->Props.AreaWidth*0.5f, this->Position.Y));
		else if (this->PButton->GetPosition().X >(this->Position.X + this->Props.AreaWidth*0.5f))
			this->PButton->SetPosition(Vector2d(this->Position.X + this->Props.AreaWidth*0.5f, this->Position.Y));

		this->Value = (this->PButton->GetPosition().X - this->Position.X + this->Props.AreaWidth * 0.5f) / this->Props.AreaWidth;
	}
	else
		this->IsChanging = false;

	if (this->VTCoeff)
		this->Super->GetFont2D()->DrawA(this->Position + this->VTPos, COLOR_WHITE_3, this->VTScale, this->IsVTFloat ? "%f" : "%d", 
		(int)(this->Value * this->VTCoeff));

	return true;

}

void ButtonSlider::SetPrimaryDescText(Vector2d& pos, float scale, char* text)
{

	this->RenderText(pos, COLOR_WHITE_3, scale, text);

}

void ButtonSlider::EnableOutValText(Vector2d& pos, float scale, float coeff, bool isFloat)
{

	this->IsVTFloat = isFloat;
	this->VTPos = pos;
	this->VTScale = scale;
	this->VTCoeff = coeff;

}

/////////////////////////////////////////////////////
//**Button funcs
/////////////////////////////////////////////////////

void BPExit(void* arg)
{
	Duality* BDF = reinterpret_cast<Duality*>(arg);
	if (BDF->GetStatus() != STATUS_MENU)
		if (BDF->GetSpace())
			BDF->GetSpace()->SaveHightScore();
	PostQuitMessage(0);
}

void BPResume(void* arg)
{
	Duality* BDF = reinterpret_cast<Duality*>(arg);
	BDF->ClearMButtons();

	for (int i = 0; i < BDF->GetElementBase()->GetElements(1).size(); i++)
	{
		if (BDF->GetElementBase()->GetElements(1).at(i)->GetEName() != "MB")
			BDF->GetElementBase()->GetElements(1).at(i)->SetIsNeedUpdate(true);
	}

	BDF->SetStatus(STATUS_GAME);
}

void BPMenu(void* arg)
{
	Duality* BDF = reinterpret_cast<Duality*>(arg);
	BDF->ClearMButtons();

	if (BDF->GetStatus() == STATUS_SETTINGS)
	{
		for (int i = 0; i < BDF->GetElementBase()->GetElements(1).size(); i++)
			ElementDelete(BDF->GetElementBase()->GetElements(1).at(i));

		BDF->DeleteBS();

	}
	else
	{
		BDF->GetSpace()->DeleteWorld();
		BDF->GetSound()->Play(std::string("desert of death.wav"), BDF->GetMenuVolume(), true);
	}
		

	BDF->InitMenu();

}

void BMGame(void* arg)
{
	Duality* BDF = reinterpret_cast<Duality*>(arg);
	BDF->InitGame();
}

void BRestart(void* arg)
{
	Duality* BDF = reinterpret_cast<Duality*>(arg);
	BDF->ClearMButtons();
	BDF->GetSpace()->DeleteWorld();
	BDF->SetStatus(STATUS_MENU_GAME);
}

void BSettings(void* arg)
{
	Duality* BDF = reinterpret_cast<Duality*>(arg);
	BDF->ClearMButtons();

	Button* NB = new Button(BDF);
	BDF->GetMButtons().push_back(NB);
	NB->SetFunc(DX_BUTTON_FUNC_TYPE_ONCLICK, &BPMenu, BDF);
	NB->Spawn(Vector2d(0, -35), 1);
	NB->RenderText(Vector2d(-20, 0), COLOR_BTEXT, 0.65f, "@B_QMENU");

	BDF->InitBS();

	BDF->SetStatus(STATUS_SETTINGS);
}

void Duality::InitBS()
{
	this->TestBS = new ButtonSlider(this, this->MenuVolume / 100.f);
	this->TestBS->Spawn(Vector2d(0, -100), 1);
	this->TestBS->EnableOutValText(Vector2d(0, 20), 0.5f, 100);
	this->TestBS->SetPrimaryDescText(Vector2d(100, 0), 0.8f, "Menu music volume");
}
void Duality::DeleteBS()
{
	ElementDelete(this->TestBS);
}

//////////////////////////////////////////////////////
//**Duality
//////////////////////////////////////////////////////

void Duality::BToggleResButtons()
{

	for (int i = 0; i < this->MButtons.size(); i++)
	{

		if (!strcmp(this->MButtons.at(i)->GetIndName(0), "R"))
		{
			this->MButtons.at(i)->ToggleVisible();
		}
	}

}

void Duality::CheckResButtons()
{

	for (int i = 0; i < this->MButtons.size(); i++)
	{

		if (!strcmp(this->MButtons.at(i)->GetIndName(0), "R"))
		{
			if (!strcmp(this->MButtons.at(i)->GetIndName(1), "86") && this->MButtons.at(i)->GetStatus(DX_BUTTON_STATUS_CLICKED))
				this->Resize(DX_DISPLAY_MODE_800_600);
			else if (!strcmp(this->MButtons.at(i)->GetIndName(1), "1276") && this->MButtons.at(i)->GetStatus(DX_BUTTON_STATUS_CLICKED))
				this->Resize(DX_DISPLAY_MODE_1280_768);
		}
	}

}

void Duality::ClearMButtons()
{

	for (int i = 0; i < this->MButtons.size(); i++)
	{
		ElementDelete(this->MButtons.at(i));
	}
	this->MButtons.clear();
	this->MButtons.shrink_to_fit();

}

bool Duality::InitApp()
{
	if (this->Device)
	{

		Duality::Init();
		
		/////////////////////////////////////
		//**Create Views
		/////////////////////////////////////

		this->SetStandartRenderSettings(D3D11_FILL_SOLID, D3D11_CULL_NONE);

		this->Input->SetWinSizes(this->WinSizes);

		/////////////////////////////////////
		//**Load Textures
		/////////////////////////////////////
		//this->SInitMaterials();
		/////////////////////////////////////
		//**Create BackGrounds
		/////////////////////////////////////

		this->InitVPorts();

		this->InitVPShaders();

		std::vector<std::string>* Files = new std::vector<std::string>;

		if (this->BSound)
			this->BSound->LoadSounds(this->Catalog, &Files);
		this->GetSound()->Play(std::string("desert of death.wav"), this->MenuVolume, true);
		this->InitMenu();


		return true;

	}

	return false;
}

void Duality::InitPause()
{

	this->Status = STATUS_GAME_PAUSE;

	for (int i = 0; i < this->ElementBase->GetElements(1).size(); i++)
	{
		if (this->ElementBase->GetElements(1).at(i)->GetEName() != "MB")
			this->ElementBase->GetElements(1).at(i)->SetIsNeedUpdate(false);
	}

	Button* NB = new Button(this);
	this->MButtons.push_back(NB);
	NB->SetFunc(DX_BUTTON_FUNC_TYPE_ONCLICK, &BPExit, this);
	NB->Spawn(Vector2d(0, -35), 1);
	NB->RenderText(Vector2d(-20, 0), COLOR_BTEXT, 0.65f, "@B_QUIT");

	NB = new Button(this);
	this->MButtons.push_back(NB);
	NB->SetFunc(DX_BUTTON_FUNC_TYPE_ONCLICK, &BPResume, this);
	NB->Spawn(Vector2d(0, 35), 1);
	NB->RenderText(Vector2d(-30, 0), COLOR_BTEXT, 0.65f, "@B_RESM");

	NB = new Button(this);
	this->MButtons.push_back(NB);
	NB->SetFunc(DX_BUTTON_FUNC_TYPE_ONCLICK, &BPMenu, this);
	NB->Spawn(Vector2d(0, 0), 1);
	NB->RenderText(Vector2d(-20, 0), COLOR_BTEXT, 0.65f, "@B_QMENU");

}

void Duality::InitMenu()
{

	Element* Back = new Element(this);
	Back->SetSizes(Vector2d(this->WinSizes.ClientWWidth, this->WinSizes.ClientWHeight));
	Back->SetMaterial(std::string("Menu"));
	Back->Spawn(Vector2d(0, 0), 1);

	this->Status = STATUS_MENU;

	Button* NB = new Button(this);
	this->MButtons.push_back(NB);
	NB->SetFunc(DX_BUTTON_FUNC_TYPE_ONCLICK, &BMGame, this);
	NB->Spawn(Vector2d(0, -35), 1);
	NB->RenderText(Vector2d(-20, 0), COLOR_BTEXT, 0.65f, "@B_SG");

	NB = new Button(this);
	this->MButtons.push_back(NB);
	NB->SetFunc(DX_BUTTON_FUNC_TYPE_ONCLICK, &BSettings, this);
	NB->Spawn(Vector2d(0, -70), 1);
	NB->RenderText(Vector2d(-35, 0), COLOR_BTEXT, 0.65f, "@B_STT");

	NB = new Button(this);
	this->MButtons.push_back(NB);
	NB->SetFunc(DX_BUTTON_FUNC_TYPE_ONCLICK, &BPExit, this);
	NB->Spawn(Vector2d(0, -105), 1);
	NB->RenderText(Vector2d(-20, 0), COLOR_BTEXT, 0.65f, "@B_QUIT");

	if (this->Space)
		D3DDelete(this->Space);

}

void Duality::InitGame()
{

	this->ClearMButtons();

	for (int i = 0; i < this->ElementBase->GetElements(1).size(); i++)
	{
		ElementDelete(this->ElementBase->GetElements(1).at(i));
	}

	this->SetRenderTarget(this->GetVPRenderTV(1));

	if (this->WinSizes.ClientWWidth < 1340 && this->WinSizes.ClientWHeight < 748)
	{
		this->Draw2D->DrawRectangle(Vector2d(0, 0), 0, Vector2d(this->WinSizes.ClientWWidth, this->WinSizes.ClientWHeight),
			1, XMFLOAT4(0, 0, 0, 0), XMFLOAT4(0, 0, 0, 1), this->GetMaterial(std::string("LoadingSD")));
	}
	else if (this->WinSizes.ClientWWidth < 1900 && this->WinSizes.ClientWHeight < 1060)
	{
		this->Draw2D->DrawRectangle(Vector2d(0, 0), 0, Vector2d(this->WinSizes.ClientWWidth, this->WinSizes.ClientWHeight),
			1, XMFLOAT4(0, 0, 0, 0), XMFLOAT4(0, 0, 0, 1), this->GetMaterial(std::string("LoadingMD")));
	}
	else
	{
		this->Draw2D->DrawRectangle(Vector2d(0, 0), 0, Vector2d(this->WinSizes.ClientWWidth, this->WinSizes.ClientWHeight),
			1, XMFLOAT4(0, 0, 0, 0), XMFLOAT4(0, 0, 0, 1), this->GetMaterial(std::string("LoadingHD")));
	}

	BasicInterface::Draw();

	if (!this->Space)
		this->Space = new World(this, 1);

	this->Space->CreateWorld();

	this->BSound->Stop(std::string("desert of death.wav"));

	this->Status = STATUS_GAME;

}

void Duality::InitGameOver()
{
	
	Button* NB = new Button(this);
	this->MButtons.push_back(NB);
	NB->SetFunc(DX_BUTTON_FUNC_TYPE_ONCLICK, &BRestart, this);
	NB->Spawn(Vector2d(0, 0), 1);
	NB->RenderText(Vector2d(-20, 0), COLOR_BTEXT, 0.65f, "@B_RS");

	NB = new Button(this);
	this->MButtons.push_back(NB);
	NB->SetFunc(DX_BUTTON_FUNC_TYPE_ONCLICK, &BPMenu ,this);
	NB->Spawn(Vector2d(0, -35), 1);
	NB->RenderText(Vector2d(-20, 0), COLOR_BTEXT, 0.65f, "@B_QMENU");

	NB = new Button(this);
	this->MButtons.push_back(NB);
	NB->SetFunc(DX_BUTTON_FUNC_TYPE_ONCLICK, &BPExit, this);
	NB->Spawn(Vector2d(0, -70), 1);
	NB->RenderText(Vector2d(-20, 0), COLOR_BTEXT, 0.65f, "@B_QUIT");

}

void Duality::GameUpdate()
{

	static float LTime = 0;

	if (this->Status == STATUS_GAME)
	{
		if (this->Input->KBClicked(VK_ESCAPE))
		{
			LTime = 0;

			this->InitPause();
			return;
		}
		

		this->Space->Update();

		if (!this->Space->GetIsPlayerAlive())
		{

			this->InitGameOver();
			this->Status = STATUS_GAME_OVER;
			return;
		}

	}
	else if (this->Status == STATUS_GAME_PAUSE)
	{
		LTime = LTime + this->Timer->GetDeltaTime();

		if (LTime >= 0)
		{
			this->Status = STATUS_PAUSE;
			return;
		}

	}
	else if (this->Status == STATUS_PAUSE)
	{

		this->Font2D->DrawA(Vector2d(-100, 100), COLOR_WHITE_3, 1.5f, "Game pause");

	}
	else if (this->Status == STATUS_MENU_GAME)
	{
		this->InitGame();
	}
	else if (this->Status == STATUS_SETTINGS)
	{
		this->Settings();
	}
	
}

void Duality::Update()
{
	
	////////////////////////////////////////////////
	//**Render Update
	////////////////////////////////////////////////

	this->ElementBase->UpdateAndDraw(this->Timer->GetDeltaTime());

	this->GameUpdate();

	if (this->Space)
		if (this->Status != STATUS_MENU)
			this->Space->Render();

	BasicInterface::Draw();

}

void Duality::Settings()
{

	this->Font2D->DrawA(Vector2d(-100, 100), COLOR_WHITE_3, 1, "No settings;)");

	if (this->TestBS->GetIsReleased())
	{
		this->MenuVolume = this->TestBS->GetValue() * 100;
		this->BSound->SetVolume(std::string("desert of death.wav"), this->MenuVolume);
	}

}

void Duality::InitVPorts()
{
	this->CreateViewPort(0, 0, this->WinSizes.ClientWWidth,
		this->WinSizes.ClientWHeight, 0, 0, "NewPS");
}

void Duality::ReleaseDefault()
{

	D3DDelete(this->Space);

	BasicInterface::ReleaseDefault();
}

void Duality::Resize(enum EDisplayModes mode)
{

	D3DAPP::Resize(mode);

	this->Input->SetWinSizes(this->WinSizes);

	this->InitVPorts();

}